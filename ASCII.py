def to_upper(letter: chr) -> chr:
    num = 0
    """
    This is a comment
    Input: Character
    Output: The upper case version of that character
    """
    num = ord(letter) - 32
    letter = chr(num)
    return letter

def to_lower(letter: chr) -> chr:
    num = 0
    """
    This is a comment
    Input:Character
    Output: the lower case version of that charcter
    """
    num = ord(letter) + 32
    letter = chr(num)
    return letter

def is_alpha(letter: chr) -> bool:
    num = 0
    """
    This is a comment
    Input: Character
    Output:True if the character is in the alphabet
    """
    num = ord(letter)
    if((num >= 97 and num <= 122) or (num >= 65 and num <= 90)):
        return True
    else:
        return False

def is_digit(letter: chr) -> bool:
    num = 0
    """
    Input: character
    Output: True if the element is a digit
    """
    num = ord(letter)
    if(num >= 48 and num <= 57):
        return True
    else:
        return False

def is_special(letter: chr) -> bool:
    num = 0
    """
    T
    Input: Chr
    Output: True if element is a special character
    """
    num = ord(letter)
    if((num >= 33 and num <= 47) or (num >= 58 and num <= 64) or (num >= 91 and num <= 96) or (num >= 123 and num <= 126)) :
        return True
    else:
        return False

print(to_upper('a'))
print(to_lower('B'))
print(is_alpha('B'))
print(is_alpha('1'))
print(is_digit('9'))
print(is_special('_'))
